<?php

class studentInfo
{
    public $ID ="004";  //property, or member variable
    public $Name="Faysal"; //property, or member variable
    public $Contact="123"; //property, or member variable

    public function set_ID($ID) //functon set
    {
         $this->ID=$ID;

    }

    public function get_ID() //function get
    {
        return $this->ID;

    }

     public function set_Name($Name)
    {
        $this->Name=$Name;
    }
    public function get_Name()
    {
        return $this->Name;
    }

    public function set_Contact($Contact)
    {
        $this->Contact=$Contact;
    }
    public function get_Contact()
    {
        return $this->Contact;
    }

}

$object = new studentInfo;
//$object->set_ID("004");
$ID= $object->get_ID();

echo $ID;
echo "<br>";


//$object->set_Name("Faysal");
$Name= $object->get_Name();
echo  $Name;
echo "<br>";

//$object->set_Contact("123");
$Contact= $object->get_Contact();
echo $Contact;
echo "<br>";


/*echo $object->ID;
echo $object->Name;
echo $object->Contact; */


